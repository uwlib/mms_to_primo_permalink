/*  Code from:
    http://stackoverflow.com/a/22050874/4731423
*/
jQuery.whenAll = function (deferreds) {
        var lastResolved = 0;

        var wrappedDeferreds = [];

        for (var i = 0; i < deferreds.length; i++) {
            wrappedDeferreds.push(jQuery.Deferred());

            deferreds[i].always(function() {
                wrappedDeferreds[lastResolved++].resolve(arguments);
            });
        }

        return jQuery.when.apply(jQuery, wrappedDeferreds).promise();
    };

/*  Code from https://github.com/wilsonhut/jquery.whereas 
    as written up at
    https://wilsonhut.wordpress.com/2013/05/30/jquery-deferred-ified-window-settimeout/

    Copyright (c) 2013 Philip C Wilson

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/

;(function($) {
    var slice = Array.prototype.slice;
    $.whereas = function( /*[func] [, delay] [, funcParameters]*/) {
        var args = arguments,
            isFunc = $.isFunction(args[0]),
            func = isFunc ? args[0] : $.noop,
            delay = (isFunc ? args[1] : args[0]) || 0,
            funcArgs = slice.call(args, 2),
            isCancelled = false,
            cancel = function(reject /* = true */) {
                clearTimeout(timerId);
                isCancelled = true;
                if ((!arguments.length || reject) && deferred.state() === "pending") {
                    deferred.rejectWith(null, funcArgs);
                }
            },
            deferred = $.Deferred(),
            timerId = setTimeout(function() {
                deferred.notifyWith(promise, funcArgs);
                if (isCancelled) {
                    return;
                }
                try {
                    var result = func.apply(this, funcArgs);
                    deferred.resolveWith(result, funcArgs);
                } catch(e) {
                    deferred.rejectWith(e, funcArgs);
                }
            }, delay),
            promise = $.extend(deferred.promise(), {
                cancel: cancel
            });
        return promise;
    };
})(jQuery);


